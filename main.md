##### Implementation and integration issues

* Server-side decryption

The CPABE Server has been implemented as a Java based web application. This makes it very difficult to integrate with JavaScript based SSE client library, and also makes it hard to distribute to users as many institutions do not allow the installation of arbitrary services on workstations.
  Because of this we have resorted to running a single CPABE Server for all users in our "Trusted Environment" along side the Registration Authority and the Keytray. The decryption of the SSE keys does thus not happen locally but on a central server.
  It is a fundamental principle of cryptography that only the owner of the appropriate key may decrypt some ciphertext. \[Understanding Cryptography, p. 6, Christof Paar & Jan Pelzl\] In our implementation, *the key owner shares their private key* with the central CPABE server in the "Trusted Environment". If this one environment gets compromised, potentially all CPABE user keys are compromised. This is a SPoF.
  The extra level of trust we now must place in this environment and the additional computational resources required is unnecessary and is purely due to an implementation detail.
  We believe the CPABE functionality should instead be a client side library with a JavaScript interface, this would allow for far better integration between the SSE and ABE aspects of ASCLEPIOS and would allow all decryption and encryption processes to take place on client devices. An approach to reimplement the CPABE functionality with a JavaScript interface has been taken but it is not fully functional yet. [\[CPABE in C\]](https://gitlab.com/asclepios-rust/c-libs/libcpabe)

* SSE does not support concurrent client

The SSE server can currently break when multiple clients search for data at the same. This bug appears to be due to a race condition. This bug could also be exploited to produce a Denial-of-Service attack for the SSE services. [\[SSETA#3\]](https://gitlab.com/asclepios-project/sseta/-/issues/3)

##### Potential Vulnerabilities

###### Vulnerabilities by design

* Central database with private keys

In the ASCLEPIOS framework Keycloak is used as a repository for ABE private (sic!) keys and these keys are stored in Keycloak's database in plaintext. This violates the fundamental principle of all asymmetric cryptography schemes that a user's private key is not shared with any other entities.
  Keycloak is thus a SPoF. If it gets compromised, an attacker can potentially decrypt all SSE keys if they can intercepted the encrypted SSE keys, e.g. extract them from client memory or retrieve them from the Keytray. This vulnerability is so bad that if exploited, the entire cryptographic protection of the data is defeated and only the algorithmic access control remains.

A *workaround* could consist of running Keycloak on a CSP which can be considered trusted or even secure. However, this would still not be a *solution* as a CSP can never be trustworthy and also because Keycloak would still be a SPoF.

We believe a better design would be to distribute user's ABE keys after creation on the air-gapped Registration Authority (see [Registration Authority as SPoF](#registration-authority-as-spof)) over a more secure channel, such as PGP encrypted email or a printable or hardware based key sent via snail-mail. Keeping a central database with all the private keys would then no longer be needed.

* No revocation

Revocation is a necessary requirement for any production ready encryption scheme but the ASCLEPIOS project did not aim to add such a mechanism to the CPABE scheme. As long as this requirement does not get fixed the CPABE scheme is incomplete and it can not be considered TRL 3 or higher because  there is no proof of concept yet that includes key revocation.

* Endpoint security

ASCLEPIOS does not suggest or implement any concept whatsoever for securing user endpoints. 

It is common among users to use insecure, outdated and closed-source software that can not be audited for third-party telemetry, data leaks or backdoors such as Microsoft Windows, Google Chrome and third-party browser addons.
  Such choices will mostly but not completely nullify the efforts of the ASCLEPIOS framework as they bypass ASCLEPIOS' encryption and could directly provide clandestine access to the decrypted data to several potentially criminal entities.
  Users also tend to take weak measures to secure their endpoint. E.g., weak or even trivial passwords are the norm rather than the exception. Many users even tend to open email attachments and follow URLs in emails in a non-sandboxed software, which is also the most common attack vector for malware. [\[BSI\]](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/Themen/Ransomware.pdf?__blob=publicationFile&v=2)

The use of ASCLEPIOS framework only makes sense if the user-operated endpoints are properly secured as well.


* Registration Authority as SPoF

In conventional public-key cryptography, each user must check the fingerprint of another user's public key before encrypting data with that public key. This check is crucial for the encryption to work properly, as otherwise man-in-the-middle (MITM) and other fatal attacks are possible. \[Understanding Cryptography, Christof Paar & Jan Pelzl, p. 154, pp. 342-344\]
  In the ASCLEPIOS framework, the Registration Authority owns a private master key which is used to create user keys which can then be distributed to the individual users.
  This scheme shifts the responsibility to check a user's (public) key signature from the decentralized network of various users towards a central authority, the Registration Authority. It is thus a SPoF, as already stated in D2.2, p. 67:

> However, it can be argued that the overall security of the system cannot rely on one
> single master authority. Furthermore, in cases where it is required to achieve a high
> level of security, a master secret key pair might be generated for each data owner.
> Such an approach could also lead to a multi-authority ABE model, as described in \[14\].

The suggested approach to to use one master key pair per data owner is not implemented and it would not provide any advantages over traditional public key encryption but keep some disadvantages (see [Bad cryptographic choices](#bad-cryptographic-choices)).

A workaround could consist of running the Registration Authority on an air-gapped system as it is much harder to compromise air-gapped systems than online system. But even this workaround would still not solve the issue as the air-gapped system would still be a SPoF.




###### Experimental software

Much of the cryptographic software in use has not been audited and lacks proper documentation.

* CPABE

The CPABE-related software consists of the `JPBC`, `cpabe-api` and`cpabe-demo` Java libraries and the `cpabe_server` and `registration-authority` Java applications.
  Large parts of that Java software are direct ports of some C software, specifically the [PBC library by the Computer Science Department at the University of Standford](https://crypto.stanford.edu/pbc/) and the [CPABE and BSWABE libraries by the Computer Science department at the University of Texas at Austin](http://acsc.cs.utexas.edu/cpabe/).

This section will discuss the original C projects first and ASCLEPIOS' Java ports after.

- TODO: maybe add that the CPABE components are not written with security-in-depth in mind? It is not strictly necessary but security-in-depth is really nice...

* PBC in C

The project is mostly but not completely unmaintained since the year 2013.
  The maintainers do not respond to issues on the project's issue tracker on GitHub.
  The project contains over 100 occurences of `TODO: ` comments that are not yet tracked with the project's issue tracker and these comments look like the project was left in a extremely early stage of development.
  Some of the project's open issue are relevant for security, e.g. `./ecc/d_param.c:  // TODO: What if there are no roots?` or `./ecc/curve.c://TODO: untested`.
  The PBC library in C seems to not be audited yet and even the compiler throws warning relevant for security, also see [PBC#28](https://github.com/blynn/pbc/issues/28).

* BSWABE/CPABE in C

The project is completely unmaintained since the year 2011.
  The library and applications use the SHA-1 hashing algorithm.
  The project seems to not be audited yet and it does not implement even a single test.
  It is mostly undocumented, as the README says:
> Right now, the only documentation that exists for this library is the
> set of comments in the header file bswabe.h.

The project website warns:
> Bugs and Limitations<br>
> None known, but like many other things on the ACSC this is research quality software and 
> should not be used in any application actually requiring security.

* JPBC

The CPABE cryptographic framework uses the [JPBC library](gas.dia.unisa.it/projects/jpbc/) which is not maintained since the year 2013.
  Neither the authors nor their university ([UNISA](https://unisa.it/), the website is currently not available because it uses a self-signed TLS certificate) respond to vulnerability reports via email.
  The implementation uses the Java `SecureRandom` library in a way that does not specify the algorithm. Thus, the library may use a PRNG, e.g. SHA1, for cryptographic operations, such as key generation. For further documentation, see [\[Report#1\]](https://gitlab.gwdg.de/snet-asclepios-demo/report-2022-6373-evaluation/-/issues/1).

* cpabe-api
The `cpabe-api` Java library is part of the [`cpabe_server`](#cpabe_server) and the [`registration-auhtority`](#registration-authority). It's code is copied and pasted between these two different git repositories.
  The project seems mostly, but not completely unmaintained since June 2021.
  The code is a direct port of the BSWABE/CPABE C software but language-specific differences have not been regarded. I.e. the Java implementation still has functions returning C-style NULL-pointers in case of failure instead of throwing exceptions. Calling code within the library does not check for NULL-pointers which results in Null Pointer Exceptions. [\[registration-authority-cpabe#20\]](https://gitlab.com/asclepios-project/registration-authority-cpabe/-/issues/20) There are more curious C-style bits like this one in the Java code.
  The software uses AES in Electronic Codebook Mode (ECB) which leaks information about the encrypted plaintext. \[Understanding Cryptography, pp. 124-128, Christof Paar & Jan Pelzl\] However, this issue is not critical as it probably only leaks information about equality and inequality of SSE keys as well as information about re-encryption of SSE keys but not the actual SSE keys themselves.
  The library uses the SHA-1 hashing algorithm.
  The project is not audited yet and it does not implement a single test.
  There is no documentation of the code but only of its interface. Such documentation is required because the code is partly difficult to read due to many variables being named only a single letter.

* cpabe-demo

There is a Demo application bundled with the `cpabe_server` and the `registration-authority` projects. It's code is copied and pasted between these two different git repositories.
  We did not do any security checks on the demo because it exists for demonstration only and is not used in practice in our demonstrator. However, it still compromises security of the `cpabe_server` and the `registration-authority`.
  However, the demo configuration file specifies a pre-defined master keypair which is not kept secret. This practice is completely fine as long as that keypair is only used for the demo and not in practice. But the demo's contents get bundled with the Docker image releases and the main applications do not have their own configuration file. They use the configuration file of the demo by default. We had quite some trouble identifying why the `cpabe_server` and `registration-authority` did not work with our own master keypair, because they default to the predefined one. If this happens in a real application, the entire CPABE scheme would be compromised. This is a SPoF.

* cpabe_server

We have already stated our opinion that the existence of the Java `cpabe_server` is a bad design decision, resulting in a SPoF. [\[Server-side decryption\]](#server-side-decryption)

It is also potentially compromised with a hardcoded master keypair by the `cpabe-demo`. [\[`cpabe-demo`\]](#cpabe-demo)

Because it uses the `cpabe-api`, it also suffers from all that's weaknesses.

Finally, this application is not audited yet and it does not implement a single test.


* registration-authority

We have already stated the fact that this component marks a SPoF. [\[Registration Authority as SPoF\]](#registration-authority-as-spof)

The design choice to deliver all user keys generated by this component to a central DB has already been criticized to mark a SPoF. [\[Central database with private keys\]](#central-database-with-private-keys).

It is also potentially compromised with a hardcoded master keypair by the `cpabe-demo`. [\[`cpabe-demo`\]](#cpabe-demo)

Because it uses the `cpabe-api`, it also suffers from all that's weaknesses.

Finally, this application is not audited yet and it does not implement a single test.


###### Bad cryptographic choices

The SHA-1 hashing algorithm is cryptographically broken [\[Leurent, Peyrin\]](https://eprint.iacr.org/2020/014.pdf), yet a search for `sha1` on all ASCLEPIOS project components' source code returns thousands of occurrences.
  RSA with 2048 bit keylength is used at several points throughout ASCLEPIOS project [\[SSETA\]](https://gitlab.com/asclepios-project/sseta/-/blob/master/teepclient/common/crypto.h#L96) [\[SNET\]](https://gitlab.gwdg.de/snet-asclepios-demo/snet-asclepios-deployment/-/blob/master/bin/generate-certs#L14) despite not being compliant with current recommendations. [\[BSI\]](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR02102/BSI-TR-02102.pdf)
  Not a single asymmetric encryption in ASCLEPIOS scheme is suitable for long-term security as the chosen key lengths are too short. E.g., RSA should be used with a key length of 15 360 bits for long-term security. [\[ENISA 2014\]](https://www.enisa.europa.eu/publications/algorithms-key-size-and-parameters-report-2014/@@download/fullReport) This key length provides a security level of 256 bits. \[Understanding Cryptography, p. 156, Christof Paar & Jan Pelzl\] The use of such long key lengths is not constrained by technical limitations today, although the particular case of CPABE may indeed require some work to extend key lengths.


###### Dependencies

Many components have complex dependency trees and pull these dependencies from untrusted and un-audited sources.

Using GitLab CI to build executable binaries means that anyone with access to GitLab can inject malicious code into the software developed at ASCLEPIOS. It is publicly known and has been confirmed by the European court that there are criminal entities with access to any US-based company. [\[Schrems II\]](https://curia.europa.eu/juris/document/document.jsf?docid=228677&doclang=en) This vulnerability applies to most ASCLEPIOS software components.
  Additionally, using Gitlab CI to deploy software to servers gives anyone with access to GitLab the chance to compromise the respective servers. This applies to most servers of the demonstrator.
  Finally, using GitLab's integrated package/container registry enables adversaries with access to GitLab to compromise packages and Docker images. A solution to this issue includes verifying the package hash values which has already been implemented partially in the demonstrator.

Some dependencies are exceptionally prone to malware injection. I.e. XNAT uses a build tool component called `gradle-git-version` which is provided by the infamous spyware producer Palantir Technologies. [\[GitHub page\]](https://github.com/palantir/gradle-git-version)



## maybe add

- vulnerable against collaboration attacks
- meta data leakage
