[19] TODO: please add quote as `[19, p. 6]` in the text instead of `[19]`
[19] Paar, C. and Pelzl, J., 2022. Understanding Cryptography. Springer-Verlag.

[20] Schulz, I., 2022. CPABE in C. [online] GitLab.com SaaS. Available at: <https://gitlab.com/asclepios-rust/c-libs/libcpabe> [Accessed 28 February 2022].

[21] Bowden, J., 2021. Denial-of-service attack possible by sending invalid data to /api/v1/searchno/<keyId_pk>/. [online] GitLab.com SaaS. Available at: <https://gitlab.com/asclepios-project/sseta/-/issues/3> [Accessed 28 February 2022].

[22] Bundesamt für Sicherheit in der Informationstechnik, 2021. Ransomware – Bedrohungslage, Prävention & Reaktion 2021. [online] Bundesamt für Sicherheit in der Informationstechnik. Available at: <https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Cyber-Sicherheit/Themen/Ransomware.pdf?__blob=publicationFile&v=2> [Accessed 28 February 2022].

[23] TODO: please remove this reference and replace all mentions with `[19, pp. 154, 342-344]`

[24] Lynn, B., 2013. PBC-Library – Paring-Based Cryptography. [online] Stanford University. Available at: <https://crypto.stanford.edu/pbc/> [Accessed 28 February 2022].

[25] Bethencourt, J., Sahai, A. and Waters, B., 2011. Advanced Crypto Software Collection. [online] University of Texas at Austin. Available at: <http://acsc.cs.utexas.edu/cpabe/> [Accessed 28 Februrary 2022].

[26] Schulz, I., 2022. possible buffer overflow. [online] GitHub, Inc. Available at: <https://github.com/blynn/pbc/issues/28> [Accessed 28 February 2022].

[27] Schulz, I., 2022. jPBC may use PRNG for CPABE crypto operations. [online] GitLab.com SaaS. Available at: <https://gitlab.gwdg.de/snet-asclepios-demo/report-2022-6373-evaluation/-/issues/1> [Accessed 28 February 2022].

[28] Schulz, I., 2021. null pointer exceptions. [online] GitLab.com SaaS. Available at: <https://gitlab.com/asclepios-project/registration-authority-cpabe/-/issues/20> [Accessed 28 February 2022].

[29] TODO: please remove this reference and replace all mentions with `[19, pp. 124-128]`

[30] Leurent, G. and Peyrin, T., 2020. SHA-1 is a Shambles – First Chosen-Prefix Collision on SHA-1 and Application to the PGP Web of Trust. [online] International Association
for Cryptologic Research. Available at: <https://eprint.iacr.org/2020/014> [Accessed 28 February 2022].

[31] Dang, H., 2021. feat: simple AES encryptor. [online] GitLab.com SaaS. Available at: <https://gitlab.com/asclepios-project/sseta/-/blob/c6ae1527bc37d80ea8119b211930be8a980a3d74/teepclient/common/crypto.h#L96> [Accessed 28 February 2022].

[32] Bundesamt für Sicherheit in der Informationstechnik, 2022. Kryptographische Verfahren: Empfehlungen und Schlüssellängen. [online] Bundesamt für Sicherheit in der Informationstechnik. Available at: <https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR02102/BSI-TR-02102.pdf> [Accessed 28 February 2022].

[33] European Union Agency for Network and Information Security, 2014. Algorithms, key size and parameters report – 2014. [online] European Union Agency for Cybersecurity. Available at: <https://www.enisa.europa.eu/publications/algorithms-key-size-and-parameters-report-2014/@@download/fullReport> [Accessed 28 February 2022].

[34] TODO: please remove this reference and replace all mentions with `[19, p. 156]`

[35] Court of Justice of the European Union (Grand Chamber), 2020. Maximilian Schrems v. Facebook Ireland Limited. [online] Court of Justice of the European Union. Available at: <https://curia.europa.eu/juris/document/document.jsf?docid=228677&doclang=en> [Accessed 28 February 2022].
